import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private router:Router
  ) { }

  ngOnInit() {
  }
  goLogin(){
    // this.router.navigate(['./detail','003'],{queryParams:{a:1,b:2}})
    this.router.navigate(['/user/login']); //路由跳转
  }

}
