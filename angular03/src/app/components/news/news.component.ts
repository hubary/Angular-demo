import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  constructor(
    private router:Router
  ) { }

  ngOnInit() {
  }
  goDetail(){
    this.router.navigate(['/news/detail','004'],{queryParams:{a:11,b:12}});//路由跳转，并传参数
  }

}
