import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  msg;
  msg2;

  constructor(
    public route:ActivatedRoute
  ) { }

  ngOnInit() {
    // console.log(this.route.snapshot.params);//组件内接参数 组件会复用

    // console.log(this.route.params); //参数订阅对象 组件不复用
    /* this.route.params.subscribe(
      params => console.log(params)
    ) */
    this.route.params.subscribe( 
      (params:Params) => this.msg=params.aid
    )

    //接受数据
    // this.route.queryParams 数据对象
    this.route.queryParams.subscribe(
      (params:Params) => this.msg2=params
    )

  }

  ngDoCheck(){
    // console.log(this.route.snapshot.params['aid']);//组件内接参数 参数快照 , 
  }

}
