import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NewsComponent } from './components/news/news.component';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/login/login.component';
import { RegComponent } from './components/reg/reg.component';
import { DetailComponent } from './components/detail/detail.component';
import { ErrorComponent } from './components/error/error.component';

let routes:Routes=[
  {path:'',redirectTo:'/home',pathMatch:'full'},//默认路由  路由跳转时要 /
  {path:'home',component:HomeComponent},//path 无需加/
  {
    path:'news',
    component:NewsComponent,
    children:[
      {path:'detail/:aid',component:DetailComponent}
    ]
  },
  {
    path:'user',
    component:UserComponent,
    children:[//子路由
      {path:'',redirectTo:'/user/reg',pathMatch:'full'},//默认路由  路由跳转时要 /
      {path:'login',component:LoginComponent},
      {path:'reg',component:RegComponent},
    ]
  },
  {path:'**',component:ErrorComponent}//错误路由
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes,{enableTracing:false})
  ],
  exports:[RouterModule],//RouterModule 路由 模块要导出
  declarations: []
})
export class RoutingModule { } //路由模块配置文件
