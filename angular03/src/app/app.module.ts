import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';



import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NewsComponent } from './components/news/news.component';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/login/login.component';
import { RegComponent } from './components/reg/reg.component';
import { DetailComponent } from './components/detail/detail.component';
import { ErrorComponent } from './components/error/error.component';

import { RoutingModule } from './routing.module';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NewsComponent,
    UserComponent,
    LoginComponent,
    RegComponent,
    DetailComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule//路由模块要导入
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
