import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  //所有的属性，创建
  title = '留言板';

  iptUserName:string='';
  iptContent:string='';
  checkContent:List;
  iptListContent:string;
  list:List[]=[
    {id:1,name:'alex1',content:'还钱1'},
    {id:2,name:'alex2',content:'还钱2'},
    {id:3,name:'alex3',content:'还钱3'},
    {id:4,name:'alex4',content:'还钱4'},
  ]

  constructor(){
    // console.log(this.checkContent);
    this.checkContent={id:-1,name:'',content:''}//构造函数可以用来初始化实例属性
  }

  add():void{
    this.list.push({
      id:this.list.length+1,
      name:this.iptUserName,
      content:this.iptContent
    });
    this.iptUserName=this.iptContent='';
  }

  del(index){
    this.list.splice(index,1);
  }

  removeAll(){
    this.list=[];
  }

  check(index){
    this.checkContent={
      index:index,
      name:this.list[index].name,
      content:this.list[index].content
    }
    // this.iptListContent=this.list[index].content;
  }
  save(){
    // this.list[this.checkContent.index].content=this.iptListContent;
    this.list[this.checkContent.index].content=this.checkContent.content
  }

}

interface List{
  id?:number,
  name:string,
  content?:string,
  index?:number
}








